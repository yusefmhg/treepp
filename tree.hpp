/*
 * Copyright (c) 2014, Yusef Mahathma Henchenski Gidrão <yusefmhg@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __TREE_HPP__
#define __TREE_HPP__

#include <cstddef>
#include <iterator>
#include <memory>
#include <queue>

template < class T, class Alloc = std::allocator<T> >
class tree{
   
public:
   typedef T value_type;
   typedef Alloc allocator_type;
   typedef value_type& reference;
   typedef const value_type& const_reference;
   typedef value_type* pointer;
   typedef const value_type* const_pointer;
   
   typedef size_t size_type;
   typedef ptrdiff_t difference_type;
   
   typedef tree<value_type,allocator_type> tree_type;
   
private:
   class tree_node_ {
   public:
      value_type data;
      tree_node_* first_child;
      tree_node_* last_child;
      tree_node_* parent;
      tree_node_* prev_sibling;
      tree_node_* next_sibling;
      
      tree_node_() :
         parent(nullptr),
         first_child(nullptr),
         last_child(nullptr),
         prev_sibling(nullptr),
         next_sibling(nullptr)
         {}
      
      tree_node_(const_reference data_value):
         data(data_value)
         {
            tree_node_();
         }
      
      tree_node_(const tree_node_ &other) :
         data(other.data),
         parent(other.parent),
         first_child(other.first_child),
         last_child(other.last_child),
         prev_sibling(other.prev_sibling),
         next_sibling(other.next_sibling)
         {}
      
      ~tree_node_(){}
      
      void set_first_child(tree_node_* node){
      if(node!=nullptr){
         node->parent=this;
         node->next_sibling=first_child;
         if(first_child!=nullptr){
            first_child->prev_sibling = node;
         } else {
            last_child = node;
         }
         first_child = node;
      }
   }
   
   void set_last_child(tree_node_* node){
      if(node!=nullptr){
         node->parent=this;
         node->prev_sibling=last_child;
         if(last_child!=nullptr){
            last_child->next_sibling = node;
         } else {
            first_child = node;
         }
         last_child = node;
      }
   }
   
   void set_next_sibling(tree_node_* node){
      if(node!=nullptr){
         node->prev_sibling = this;
         node->next_sibling = next_sibling;
         node->parent = parent;
         if(next_sibling!=nullptr){
            next_sibling->prev_sibling = node;
         } else {
            parent->last_child = node;
         }
         next_sibling = node;
      }
   }
   
   void set_prev_sibling(tree_node_* node){
      if(node!=nullptr){
         node->next_sibling = this;
         node->prev_sibling = prev_sibling;
         node->parent = parent;
         if(prev_sibling!=nullptr){
            prev_sibling->next_sibling = node;
         } else {
            parent->first_child = node;
         }
         prev_sibling = node;
      }
   }
   };
   
   typedef tree_node_* node_type;
   typedef std::allocator<tree_node_> node_allocator_type;
   typedef std::iterator<std::bidirectional_iterator_tag, value_type> iterator_category;
   class iterator_base : public iterator_category {
      friend class tree;
   protected:
      node_type node;
      
      iterator_base(node_type nd) : node(nd) {}
      
   public:
      
      iterator_base() : node(nullptr) {}
      iterator_base(const iterator_base& other) : node(other.node) {}
      
      iterator_base& operator= (const iterator_base& other){
         node = other.node;
         return *this;
      }
      
      bool operator== (const iterator_base& other) const {
         return  iterator_base::node==other.node;
      }
      
      inline bool operator!= (const iterator_base& other) const {
         return !(*this==other);
      }
      
      reference operator*() const {
         return node->data;
      }
      
      inline pointer operator->() const {
         return &operator*();
      }
   };
   
public:
   
   class depth_iterator : public iterator_base {
      friend class tree;
   protected:
      using iterator_base::node;
      depth_iterator(node_type nd) : iterator_base(nd) {}
      
   private:
      inline void increment() {
         if (node!=nullptr) {
            if (node->first_child!=nullptr) {
               node = node->first_child;
            } else {
               while (node->next_sibling==nullptr && node->parent!=nullptr) {
                  node = node->parent;
               }
               if(node->next_sibling!=nullptr){
                  node = node->next_sibling;
               }
            }
         }
      }
      
      inline void decrement() {
         if(node!=nullptr){
            if(node->prev_sibling!=nullptr){
               node = node->prev_sibling;
               while(node->last_child!=nullptr){
                  node = node->last_child;
               }
            } else if (node->parent != nullptr){
               node = node->parent;
            }
         }
      }
      
   public:
      depth_iterator() : iterator_base() {};
      depth_iterator(const iterator_base& other) : iterator_base(other) {};
      
      depth_iterator& operator++(){
         increment();
         return *this;
      }
      
      depth_iterator operator++(int){
         depth_iterator result(*this);
         increment();
         return result;
      }
      
      depth_iterator& operator--(){
         decrement();
         return *this;
      }
      
      depth_iterator operator--(int){
         depth_iterator result(*this);
         decrement();
         return result;
      }
      
   };

   typedef const depth_iterator const_depth_iterator;
   typedef std::reverse_iterator<depth_iterator> reverse_depth_iterator;
   typedef std::reverse_iterator<const_depth_iterator> const_reverse_depth_iterator;
   
   class post_depth_iterator : public iterator_base {
      friend class tree;
   protected:
      using iterator_base::node;
      post_depth_iterator(node_type nd) : iterator_base(nd) {}
      
   private:
      inline void increment() {
         if (node!=nullptr) {
            if (node->next_sibling!=nullptr) {
               node = node->next_sibling;
               while(node->first_child!=nullptr){
                  node = node->first_child;
               }
            } else if (node->parent != nullptr) {
               node = node->parent;
            }
         }
      }
      
      inline void decrement() {
         if (node!=nullptr) {
            if (node->last_child!=nullptr) {
               node = node->last_child;
            } else {
               while (node->prev_sibling==nullptr && node->parent!=nullptr) {
                  node = node->parent;
               }
               if(node->prev_sibling!=nullptr){
                  node = node->prev_sibling;
               }
            }
         }
      }
      
   public:
      post_depth_iterator() : iterator_base() {}
      post_depth_iterator(const iterator_base& other) : iterator_base(other) {};
      
      post_depth_iterator& operator++(){
         increment();
         return *this;
      }
      
      post_depth_iterator operator++(int){
         post_depth_iterator result(*this);
         increment();
         return result;
      }
      
      post_depth_iterator& operator--(){
         decrement();
         return *this;
      }
      
      post_depth_iterator operator--(int){
         post_depth_iterator result(*this);
         decrement();
         return result;
      }
      
   };
   typedef const post_depth_iterator const_post_depth_iterator;
   typedef std::reverse_iterator<post_depth_iterator> reverse_post_depth_iterator;
   typedef std::reverse_iterator<const_post_depth_iterator> const_reverse_post_depth_iterator;
   
   class children_iterator : public iterator_base {
      friend class tree;
   protected:
      using iterator_base::node;
      
      node_type parent;
      
      children_iterator(node_type nd) : iterator_base(nd), parent(nd->parent) {}
      
   private:
      inline void increment() {
         if (node!=nullptr) {
            if(node->parent != parent){
               if(node->next_sibling!=nullptr){
                  node = parent->first_child;
               }
            } else {
               if(node->next_sibling == nullptr){
                  while(node->parent!=nullptr) node = node->parent;
               }
               node = node->next_sibling;
            }
         }
      }
      
      inline void decrement() {
         if (node!=nullptr) {
            if(node->parent != parent){
               if(node->prev_sibling!=nullptr){
                  node = parent->last_child;
               }
            } else {
               if(node->prev_sibling == nullptr){
                  while(node->parent!=nullptr) node = node->parent;
               }
               node = node->prev_sibling;
            }
         }
      }
      
   public:
      children_iterator() : iterator_base(), parent(nullptr) {};
      children_iterator(const iterator_base& other) :
         iterator_base(other),
         parent(other.node->parent)
         {};
      
      children_iterator& operator++(){
         increment();
         return *this;
      }
      
      children_iterator operator++(int){
         children_iterator result(*this);
         increment();
         return result;
      }
      
      children_iterator& operator--(){
         decrement();
         return *this;
      }
      
      children_iterator operator--(int){
         children_iterator result(*this);
         decrement();
         return result;
      }
      
      children_iterator& operator= (const iterator_base& other){
         iterator_base::operator=(other);
         parent = node->parent;
         return *this;
      }
      
   };
   typedef const children_iterator const_children_iterator;
   typedef std::reverse_iterator<children_iterator> reverse_children_iterator;
   typedef std::reverse_iterator<const_children_iterator> const_reverse_children_iterator;
   
   class leaf_iterator : public iterator_base {
      friend class tree;
   protected:
      using iterator_base::node;
      leaf_iterator(node_type nd) : iterator_base(nd) {}
      
   private:
      inline void increment() {
         if (node!=nullptr) {
            while (node->next_sibling==nullptr && node->parent!=nullptr) {
               node = node->parent;
            }
            if(node->next_sibling!=nullptr){
               node = node->next_sibling;
            }
            while(node->first_child!=nullptr){
               node = node->first_child;
            }
         }
      }
      
      inline void decrement() {
         if(node!=nullptr){
            while (node->prev_sibling==nullptr && node->parent!=nullptr) {
               node = node->parent;
            }
            if(node->prev_sibling!=nullptr){
               node = node->prev_sibling;
            }
            while(node->last_child!=nullptr){
               node = node->last_child;
            }
         }
      }
      
   public:
      leaf_iterator() : iterator_base() {};
      leaf_iterator(const iterator_base& other) : iterator_base(other) {};
      
      leaf_iterator& operator++(){
         increment();
         return *this;
      }
      
      leaf_iterator operator++(int){
         leaf_iterator result(*this);
         increment();
         return result;
      }
      
      leaf_iterator& operator--(){
         decrement();
         return *this;
      }
      
      leaf_iterator operator--(int){
         leaf_iterator result(*this);
         decrement();
         return result;
      }
      
   };
   typedef const leaf_iterator const_leaf_iterator;
   typedef std::reverse_iterator<leaf_iterator> reverse_leaf_iterator;
   typedef std::reverse_iterator<const_leaf_iterator> const_reverse_leaf_iterator;
   
private:
   
   node_type top, bottom;  /* top and bottom are both dummy nodes. Dereferencing a iterator
                              pointing to any of them has a undefined behavior.              */
   node_allocator_type node_allocator;
   
   inline void top_bottom_init(){
      top = node_allocator.allocate(1);
      bottom = node_allocator.allocate(1);
      
      node_allocator.construct(top);
      node_allocator.construct(bottom);
      
      top->next_sibling = bottom;
      bottom->prev_sibling = top;
   }
   
   inline depth_iterator plant_root(const_reference root_data){
      if(top->next_sibling==bottom) {
         return insert_next_sibling(top, root_data);
      } else {
         return root();
      }
   }
   
   inline void copy(const tree_type&);
      
public:
   
   tree() {
      top_bottom_init();
   }
   
   tree(const_reference root_data) {
      top_bottom_init();
      plant_root(root_data);
   }
   
   tree(const tree_type& other){
      top_bottom_init();
      copy(other);
   }
   
   ~tree() {
      clear();
      node_allocator.deallocate(top, 1);
      node_allocator.deallocate(bottom, 1);
   }
   
   iterator_base root() noexcept {
      return top->next_sibling;
   }
   const iterator_base croot() const noexcept {
      return top->next_sibling;
   }
   inline const iterator_base root() const noexcept {
      return croot();
   }
   
   depth_iterator begin() noexcept {
      return top->next_sibling;
   }
   const_depth_iterator cbegin() const noexcept {
      return top->next_sibling;
   }
   inline const_depth_iterator begin() const noexcept {
      return cbegin();
   }
   reverse_depth_iterator rbegin() noexcept {
      return end();
   }
   const_reverse_depth_iterator crbegin() const noexcept {
      return cend();
   }
   inline const_reverse_depth_iterator rbegin() const noexcept {
      return crbegin();
   }
   depth_iterator end(){
      return bottom;
   }
   const_depth_iterator cend() const noexcept {
      return bottom;
   }
   inline const_depth_iterator end() const noexcept {
      return cend();
   }
   reverse_depth_iterator rend() noexcept {
      return begin();
   }
   const_reverse_depth_iterator crend() const noexcept {
      return cbegin();
   }
   inline const_reverse_depth_iterator rend() const noexcept {
      return crend();
   }
   
   post_depth_iterator post_depth_begin() noexcept {
      return top->next_sibling;
   }
   const_post_depth_iterator post_depth_cbegin() const noexcept {
      return top->next_sibling;
   }
   inline const_post_depth_iterator post_depth_begin() const noexcept {
      return post_depth_cbegin();
   }
   reverse_post_depth_iterator post_depth_rbegin() noexcept {
      return post_depth_end();
   }
   const_reverse_post_depth_iterator post_depth_crbegin() const noexcept {
      return post_depth_cend();
   }
   inline const_reverse_post_depth_iterator post_depth_rbegin() const noexcept {
      return post_depth_crbegin();
   }
   post_depth_iterator post_depth_end() {
      return bottom;
   }
   const_post_depth_iterator post_depth_cend() const noexcept {
      return bottom;
   }
   inline const_post_depth_iterator post_depth_end() const noexcept {
      return post_depth_cend();
   }
   reverse_post_depth_iterator post_depth_rend() noexcept {
      return post_depth_begin();
   }
   const_reverse_post_depth_iterator post_depth_crend() const noexcept {
      return post_depth_cbegin();
   }
   inline const_reverse_post_depth_iterator post_depth_rend() const noexcept {
      return post_depth_crend();
   }
   
   leaf_iterator leaf_begin(const iterator_base&) noexcept;
   const_leaf_iterator leaf_cbegin(const iterator_base&) const noexcept;
   inline const_leaf_iterator leaf_begin(const iterator_base& subtree_root) const noexcept {
      return leaf_cbegin(subtree_root);
   }
   leaf_iterator leaf_rbegin(const iterator_base&) noexcept;
   const_leaf_iterator leaf_crbegin(const iterator_base&) const noexcept;
   inline const_leaf_iterator leaf_rbegin(const iterator_base& subtree_root) const noexcept {
      return leaf_crbegin(subtree_root);
   }
   leaf_iterator leaf_end(const iterator_base&) noexcept;
   const_leaf_iterator leaf_cend(const iterator_base&) const noexcept;
   inline const_leaf_iterator leaf_end(const iterator_base& subtree_root) const noexcept {
      return leaf_cend(subtree_root);
   }
   leaf_iterator leaf_rend(const iterator_base&) noexcept;
   const_leaf_iterator leaf_crend(const iterator_base&) const noexcept;
   inline const_leaf_iterator leaf_rend(const iterator_base& subtree_root) const noexcept {
      return leaf_crend(subtree_root);
   }
   
   leaf_iterator leaf_begin() noexcept;
   const_leaf_iterator leaf_cbegin() const noexcept;
   inline const_leaf_iterator leaf_begin() const noexcept {
      return leaf_cbegin();
   }
   leaf_iterator leaf_rbegin() noexcept;
   const_leaf_iterator leaf_crbegin() const noexcept;
   inline const_leaf_iterator leaf_rbegin() const noexcept {
      return leaf_crbegin();
   }
   leaf_iterator leaf_end() noexcept;
   const_leaf_iterator leaf_cend() const noexcept;
   inline const_leaf_iterator leaf_end() const noexcept {
      return leaf_cend();
   }
   leaf_iterator leaf_rend() noexcept;
   const_leaf_iterator leaf_crend() const noexcept;
   inline const_leaf_iterator leaf_rend() const noexcept {
      return leaf_crend();
   }
   
   children_iterator children_begin(const iterator_base& parent) noexcept {}
   const_children_iterator children_cbegin(const iterator_base& parent) const noexcept{}
   inline const_children_iterator children_begin(const iterator_base& parent) const noexcept {}
   reverse_children_iterator children_rbegin(const iterator_base& parent) noexcept {}
   const_reverse_children_iterator children_crbegin(const iterator_base& parent) const noexcept {}
   inline const_reverse_children_iterator children_rbegin(const iterator_base& parent) const noexcept {}
   children_iterator children_end(const iterator_base& parent) noexcept {}
   const_children_iterator children_cend(const iterator_base& parent) const noexcept {}
   inline const_children_iterator children_end(const iterator_base& parent) const noexcept {}
   children_iterator children_rend(const iterator_base& parent) noexcept {}
   const_reverse_children_iterator children_crend(const iterator_base& parent) const noexcept {}
   inline const_children_iterator children_rend(const iterator_base& parent) const noexcept {}
   
   bool empty() const noexcept {
      return top->next_sibling==bottom;
   }
   
   size_type size() const noexcept;
   size_type size(const iterator_base&) const noexcept;
   
   size_type depth(const iterator_base&) const noexcept;
   difference_type depth(const iterator_base&, const iterator_base&) const noexcept;
   
   size_type max_depth() const noexcept;
   size_type max_depth(const iterator_base&) const noexcept;
   
   size_type number_of_children(const iterator_base&) const;
   
   bool is_valid(const iterator_base& it) const noexcept { return it.node!=nullptr; }
   
   template<class iterator> iterator erase(iterator&& it){
      node_type current_node = it.node;
      iterator result = it;
      erase_children(it);
      
      ++result;
      
      if(current_node!=bottom && current_node->prev_sibling!=nullptr){
         current_node->prev_sibling->next_sibling = current_node->next_sibling;
      } else if(current_node->parent!=nullptr){
         current_node->parent->first_child = current_node->next_sibling;
      }
      
      if(current_node!=top && current_node->next_sibling!=nullptr){
         current_node->next_sibling->prev_sibling = current_node->prev_sibling;
      } else if(current_node->parent!=nullptr){
         current_node->parent->last_child = current_node->prev_sibling;
      }
      
      node_allocator.deallocate(current_node, 1);
      return result;
   }
   inline void clear(){
      while (!empty()){
         erase(depth_iterator(top->next_sibling));
      }
   }
   void erase_children(const iterator_base& it){
      node_type node;
      if(is_valid(it)){
         node = it.node;
         while(node->first_child!=nullptr){
            erase(children_begin(it));
         }
      }
   }
   
   depth_iterator insert_first_child(const iterator_base& parent, const_reference data){
      node_type new_node = nullptr;
      if(is_valid(parent)){
         new_node = node_allocator.allocate(1, parent.node);
         node_allocator.construct(new_node, data);
         parent.node->set_first_child(new_node);
      }
      return new_node;
   }
   
   depth_iterator insert_last_child(const iterator_base& parent, const_reference data){
      node_type prev_sibling;
      node_type new_node = nullptr;
      if(is_valid(parent)){
         new_node = node_allocator.allocate(1, parent.node);
         node_allocator.construct(new_node, data);
         parent.node->set_last_child(new_node);
      }
      return new_node;
   }
   
   inline depth_iterator insert_child(const iterator_base& parent, const_reference data){
      return insert_last_child(parent, data);
   }
   
   depth_iterator insert_next_sibling(const iterator_base& sibling, const_reference data){
      node_type new_node = nullptr;
      if(is_valid(sibling) || empty() && sibling.node==top){
         new_node = node_allocator.allocate(1, sibling.node);
         node_allocator.construct(new_node, data);
         sibling.node->set_next_sibling(new_node);
      }
      return new_node;
   }
   
   depth_iterator insert_prev_sibling(const iterator_base& sibling, const_reference data){
      node_type new_node = nullptr;
      if(is_valid(sibling) || empty() && sibling.node==bottom){
         new_node = node_allocator.allocate(1, sibling.node);
         node_allocator.construct(new_node, data);
         sibling.node->set_prev_sibling(new_node);
      }
      return new_node;
   }
   
   inline depth_iterator insert_sibling(const iterator_base& sibling_it, const_reference data){
      return insert_next_sibling(sibling_it, data);
   }
   
   depth_iterator append_first_child(const iterator_base& parent, const tree_type& subtree) {
      node_type node = nullptr;
      if( is_valid(parent) && !subtree.empty() ){
         node = node_allocator.allocate(1, parent.node);
         node_allocator.construct(node,*(subtree.top->next_sibling));
         parent.node->set_first_child(node);
      }
      return node;
   }
   
   depth_iterator append_first_child(const iterator_base& parent, tree_type&& subtree) {
      node_type node = nullptr;
      if( is_valid(parent) && !subtree.empty() ){
         node = subtree.top->next_sibling;
         node->next_sibling = nullptr;
         node->prev_sibling = nullptr;
         parent.node->set_first_child(node);
         subtree.top->next_sibling = subtree.bottom;
         subtree.bottom->prev_sibling = subtree.top;
      }
      return node;
   }
   
   depth_iterator append_last_child(const iterator_base& parent, const tree_type& subtree) {
      node_type node = nullptr;
      if( is_valid(parent) && !subtree.empty() ){
         node = node_allocator.allocate(1, parent.node);
         node_allocator.construct(node,*(subtree.top->next_sibling));
         parent.node->set_last_child(node);
      }
      return node;
   }
   
   depth_iterator append_last_child(const iterator_base& parent, tree_type&& subtree) {
      node_type node = nullptr;
      if( is_valid(parent)  && !subtree.empty() ){
         node = subtree.top->next_sibling;
         node->next_sibling = nullptr;
         node->prev_sibling = nullptr;
         parent.node->set_last_child(node);
         subtree.top->next_sibling = subtree.bottom;
         subtree.bottom->prev_sibling = subtree.top;
      }
      return node;
   }
   
   inline depth_iterator append_child(const iterator_base& parent, const tree_type& subtree){
      return append_last_child(parent, subtree);
   }
   
   depth_iterator append_next_sibling(const iterator_base& sibling, const tree_type& subtree) {
      node_type node = nullptr;
      if( (is_valid(sibling) || empty() && sibling.node==top) && !subtree.empty() ){
         node = node_allocator.allocate(1, sibling.node);
         node_allocator.construct(node,*(subtree.top->next_sibling));
         sibling.node->set_next_sibling(node);
      }
      return node;
   }
   
   depth_iterator append_next_sibling(const iterator_base& sibling, tree_type&& subtree) {
      node_type node = nullptr;
      if( (is_valid(sibling) || empty() && sibling.node==top) && !subtree.empty() ){
         node = subtree.top->next_sibling;
         node->next_sibling = nullptr;
         node->prev_sibling = nullptr;
         sibling.node->set_next_sibling(node);
         subtree.top->next_sibling = subtree.bottom;
         subtree.bottom->prev_sibling = subtree.top;
      }
      return node;
   }
   
   depth_iterator append_prev_sibling(const iterator_base& sibling, const tree_type& subtree) {
      node_type node = nullptr;
      if( (is_valid(sibling) || empty() && sibling.node==top) && !subtree.empty() ){
         node = node_allocator.allocate(1, sibling.node);
         node_allocator.construct(node,*(subtree.top->next_sibling));
         sibling.node->set_prev_sibling(node);
      }
      return node;
   }
   
   depth_iterator append_prev_sibling(const iterator_base& sibling, tree_type&& subtree) {
      node_type node = nullptr;
      if( (is_valid(sibling) || empty() && sibling.node==top) && !subtree.empty() ){
         node = subtree.top->next_sibling;
         node->next_sibling = nullptr;
         node->prev_sibling = nullptr;
         sibling.node->set_prev_sibling(node);
         subtree.top->next_sibling = subtree.bottom;
         subtree.bottom->prev_sibling = subtree.top;
      }
      return node;
   }
   
   inline depth_iterator append_sibling(const iterator_base& sibling, const tree_type& subtree){
      return append_next_sibling(sibling, subtree);
   }
};

#endif