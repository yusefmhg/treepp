/*
 * Copyright (c) 2014, Yusef Mahathma Henchenski Gidrão <yusefmhg@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __SIMPLE_TREE_HPP__
#define __SIMPLE_TREE_HPP__

#include <cstddef>
#include <iterator>
#include <memory>

template<class T> class tree_node_ {   
public:
   T data;
   tree_node_* first_child;
   tree_node_* last_child;
   tree_node_* parent;
   tree_node_* prev_sibling;
   tree_node_* next_sibling;
   
   tree_node_() :
      parent(nullptr),
      first_child(nullptr),
      last_child(nullptr),
      prev_sibling(nullptr),
      next_sibling(nullptr)
      {}
   
   tree_node_(const T& data_value):
      data(data_value),
      parent(nullptr),
      first_child(nullptr),
      last_child(nullptr),
      prev_sibling(nullptr),
      next_sibling(nullptr)
      {}
   
   tree_node_(tree_node_&& other):
      data(other.data),
      parent(other.parent),
      first_child(other.first_child),
      last_child(other.last_child),
      prev_sibling(other.prev_sibling),
      next_sibling(other.next_sibling)
      {}
   
   tree_node_(const tree_node_& other):
      data(other.data), parent(nullptr),
      first_child(nullptr), last_child(nullptr),
      prev_sibling(nullptr), next_sibling(nullptr)
   {  static std::allocator<tree_node_> node_allocator;
      tree_node_* temp;
      for(tree_node_* curr=other.first_child; curr!=nullptr; curr=curr->next_sibling){
         temp = node_allocator.allocate(1, this);
         node_allocator.construct(temp, *curr);
         set_last_child(temp);
      }
   }
   
   ~tree_node_(){}
   
   void set_first_child(tree_node_* node){
      if(node!=nullptr){
         node->parent=this;
         node->next_sibling=first_child;
         if(first_child!=nullptr){
            first_child->prev_sibling = node;
         } else {
            last_child = node;
         }
         first_child = node;
      }
   }
   
   void set_last_child(tree_node_* node){
      if(node!=nullptr){
         node->parent=this;
         node->prev_sibling=last_child;
         if(last_child!=nullptr){
            last_child->next_sibling = node;
         } else {
            first_child = node;
         }
         last_child = node;
      }
   }
   
   void set_next_sibling(tree_node_* node){
      if(node!=nullptr){
         node->prev_sibling = this;
         node->next_sibling = next_sibling;
         node->parent = parent;
         if(next_sibling!=nullptr){
            next_sibling->prev_sibling = node;
         } else {
            parent->last_child = node;
         }
         next_sibling = node;
      }
   }
   
   void set_prev_sibling(tree_node_* node){
      if(node!=nullptr){
         node->next_sibling = this;
         node->prev_sibling = prev_sibling;
         node->parent = parent;
         if(prev_sibling!=nullptr){
            prev_sibling->next_sibling = node;
         } else {
            parent->first_child = node;
         }
         prev_sibling = node;
      }
   }
   
};

template < class T, class Alloc = std::allocator<tree_node_<T> > >
class tree{
   
public:
   typedef T value_type;
   typedef Alloc allocator_type;
   typedef value_type& reference;
   typedef const value_type& const_reference;
   typedef value_type* pointer;
   typedef const value_type* const_pointer;
   
   typedef size_t size_type;
   typedef ptrdiff_t difference_type;
   
   
protected:
   typedef tree<value_type,allocator_type> tree_type;
   typedef tree_node_<value_type>* node_type;
   typedef std::iterator<std::bidirectional_iterator_tag, value_type> iterator_category;
   
public:
   class iterator : public iterator_category {
      
      friend tree;
      
      inline void increment() {
         if (node!=nullptr) {
            if (node->first_child!=nullptr) {
               node = node->first_child;
            } else {
               while (node->next_sibling==nullptr && node->parent!=nullptr) {
                  node = node->parent;
               }
               if(node->next_sibling!=nullptr){
                  node = node->next_sibling;
               }
            }
         }
      }
      
      inline void decrement() {
         if(node!=nullptr){
            if(node->prev_sibling!=nullptr){
               node = node->prev_sibling;
               while(node->last_child!=nullptr){
                  node = node->last_child;
               }
            } else if (node->parent != nullptr){
               node = node->parent;
            }
         }
      }
      
   protected:
      node_type node;
      iterator(node_type nd) : node(nd) {}
      
   public:
      
      iterator(const iterator& other) : node(other.node) {}
      
      iterator& operator= (const iterator& other){
         node = other.node;
         return *this;
      }
      
      bool operator== (const iterator& other) const {
         return  node==other.node;
      }
      
      inline bool operator!= (const iterator& other) const {
         return !(*this==other);
      }
      
      reference operator*() const {
         return node->data;
      }
      
      inline pointer operator->() const {
         return &operator*();
      }
      
      iterator& operator++(){
         increment();
         return *this;
      }
      
      iterator operator++(int){
         iterator result(*this);
         increment();
         return result;
      }
      
      iterator& operator--(){
         decrement();
         return *this;
      }
      
      iterator operator--(int){
         iterator result(*this);
         decrement();
         return result;
      }
      
   };

   typedef const iterator const_iterator;
   typedef std::reverse_iterator<iterator> reverse_iterator;
   typedef std::reverse_iterator<const_iterator> const_reverse_iterator;
   
private:
   
   node_type top, bottom;  /* top and bottom are both dummy nodes. Dereferencing a iterator
                              pointing to any of them has a undefined behavior.              */
   allocator_type node_allocator;
   
   inline void top_bottom_init(){
      top = node_allocator.allocate(1);
      bottom = node_allocator.allocate(1);
      
      node_allocator.construct(top);
      node_allocator.construct(bottom);
      
      top->next_sibling = bottom;
      bottom->prev_sibling = top;
   }
   
   inline iterator plant_root(const_reference root_data){
      node_type new_root;
      if(empty()) {
         new_root = node_allocator.allocate(1, top);
         node_allocator.construct(new_root, root_data);
         
         new_root->prev_sibling = top;
         top->next_sibling = new_root;
         
         new_root->next_sibling = bottom;
         bottom->prev_sibling = new_root;
      }
      return root();
   }
   
   void copy(const tree_type& other) {
      node_type node;
      clear();
      if(!other.empty()){
         node = node_allocator.allocate(1, top);
         node_allocator.construct(node, *(other.top->next_sibling));
         node->prev_sibling = top;
         node->next_sibling = bottom;
         top->next_sibling = node;
         bottom->prev_sibling = node;
      }
   }
      
public:
   
   tree() {
      top_bottom_init();
   }
   
   tree(const_reference root_data) {
      top_bottom_init();
      plant_root(root_data);
   }
     
   tree(const tree_type& other){
      top_bottom_init();
      copy(other);
   }
   
   tree(tree_type&& other){
      top_bottom_init();
      if(!other.empty()){
         top->next_sibling = other.top->next_sibling;
         bottom->prev_sibling = other.bottom->prev_sibling;
         other.top->next_sibling = other.bottom;
         other.bottom->prev_sibling = other.top;
      }
   }
   
   ~tree() {
      clear();
      node_allocator.deallocate(top, 1);
      node_allocator.deallocate(bottom, 1);
   }
   
   iterator root() noexcept {
      return iterator(top->next_sibling);
   }
   
   const_iterator croot() const noexcept {
      return const_iterator(top->next_sibling);
   }
   
   inline const_iterator root() const noexcept {
      return croot();
   }
   
   inline iterator begin() noexcept {
      return root();
   }
   
   inline const_iterator cbegin() const noexcept {
      return croot();
   }
   
   inline const_iterator begin() const noexcept {
      return cbegin();
   }
   
   reverse_iterator rbegin() noexcept {
      return reverse_iterator(end());
   }
   
   const_reverse_iterator crbegin() const noexcept {
      return const_reverse_iterator(end());
   }
   
   inline const_reverse_iterator rbegin() const noexcept {
      return crbegin();
   }
   
   iterator end(){
      return iterator(bottom);
   }
   
   const_iterator cend() const noexcept {
      return const_iterator(bottom);
   }
   
   inline const_iterator end() const noexcept {
      return cend();
   }
   
   reverse_iterator rend() noexcept {
      return reverse_iterator(begin());
   }
   
   const_reverse_iterator crend() const noexcept {
      return const_reverse_iterator(begin());
   }
   
   inline const_reverse_iterator rend() const noexcept {
      return crend();
   }
   
   bool empty() const noexcept {
      return top->next_sibling==bottom;
   }
   
   inline size_type size() const noexcept {
      return size(root());
   }
   
   size_type size(const_iterator& local_root) const noexcept {
      node_type node;
      size_type result=0;
      if(is_valid(local_root)){
         result+=1;
         for(node=local_root.node->first_child;node!=nullptr;node=node->next_sibling){
            result += size(iterator(node));
         }
      }
      return result;
   }
   
   size_type depth(const_iterator& it) const noexcept {
      size_type result = 0;
      node_type node = it.node;
      if(is_valid(it)){
         while(node != nullptr){
            ++result;
            node = node->parent;
         }
      }
      return result;
   }
   
   inline size_type max_depth() const noexcept {
      return max_depth(root());
   }
   
   size_type max_depth(const_iterator& it) const noexcept {
      node_type node;
      size_type current_children_depth;
      size_type result=0;
      if(is_valid(it)){
         node = it.node->first_child;
         while(node!=nullptr){
            current_children_depth = max_depth(node);
            if(current_children_depth > result){
               result = current_children_depth;
            }
            node=node->next_sibling;
         }
         ++result;
      }
      return result;
   }
   
   size_type number_of_children(const_iterator& it) const {
      node_type node;
      size_type result = 0;
      for (node=it.node->first_child; node!=nullptr; node = node->next_sibling){
         ++result;
      }
      return result;
   }
   
   bool is_valid(const_iterator& it) const noexcept {
      return ( it.node!=nullptr && it.node!=top && it.node!=bottom );
   }
   
   void erase(const_iterator& it){
      node_type node = it.node;
      if(is_valid(it)){
         if(node->next_sibling!=nullptr){
            node->next_sibling->prev_sibling = node->prev_sibling;
         } else if (node->parent!=nullptr) {
            node->parent->last_child = node->prev_sibling;
         }
         if(node->prev_sibling!=nullptr){
            node->prev_sibling->next_sibling = node->next_sibling;
         } else if (node->parent!=nullptr) {
            node->parent->first_child = node->next_sibling;
         }
         erase_children(it);
         node_allocator.deallocate(node, 1);
      }
   }
   
   inline void clear(){
      while (!empty()){
         erase(root());
      }
   }
   
   void erase_children(const_iterator& it){
      node_type node = it.node;
      if(is_valid(it)){
         while(node->first_child!=nullptr){
            erase(iterator(node->first_child));
         }
      }
   }
   
   iterator insert_first_child(const_iterator& parent, const_reference data){
      node_type new_node = nullptr;
      if(is_valid(parent)){
         new_node = node_allocator.allocate(1, parent.node);
         node_allocator.construct(new_node, data);
         parent.node->set_first_child(new_node);
      }
      return new_node;
   }
   
   iterator insert_last_child(const_iterator& parent, const_reference data){
      node_type prev_sibling;
      node_type new_node = nullptr;
      if(is_valid(parent)){
         new_node = node_allocator.allocate(1, parent.node);
         node_allocator.construct(new_node, data);
         parent.node->set_last_child(new_node);
      }
      return new_node;
   }
   
   inline iterator insert_child(const_iterator& parent, const_reference data){
      return insert_last_child(parent, data);
   }
   
   iterator insert_next_sibling(const_iterator& sibling, const_reference data){
      node_type new_node = nullptr;
      if(is_valid(sibling) || empty() && sibling.node==top){
         new_node = node_allocator.allocate(1, sibling.node);
         node_allocator.construct(new_node, data);
         sibling.node->set_next_sibling(new_node);
      }
      return new_node;
   }
   
   iterator insert_prev_sibling(const_iterator& sibling, const_reference data){
      node_type new_node = nullptr;
      if(is_valid(sibling) || empty() && sibling.node==bottom){
         new_node = node_allocator.allocate(1, sibling.node);
         node_allocator.construct(new_node, data);
         sibling.node->set_prev_sibling(new_node);
      }
      return new_node;
   }
   
   inline iterator insert_sibling(const_iterator& sibling_it, const_reference data){
      return insert_next_sibling(sibling_it, data);
   }
   
   iterator append_first_child(const_iterator& parent, const tree_type& subtree) {
      node_type node = nullptr;
      if( is_valid(parent) && !subtree.empty() ){
         node = node_allocator.allocate(1, parent.node);
         node_allocator.construct(node,*(subtree.top->next_sibling));
         parent.node->set_first_child(node);
      }
      return node;
   }
   
   iterator append_first_child(const_iterator& parent, tree_type&& subtree) {
      node_type node = nullptr;
      if( is_valid(parent) && !subtree.empty() ){
         node = subtree.top->next_sibling;
         node->next_sibling = nullptr;
         node->prev_sibling = nullptr;
         parent.node->set_first_child(node);
         subtree.top->next_sibling = subtree.bottom;
         subtree.bottom->prev_sibling = subtree.top;
      }
      return node;
   }
   
   iterator append_last_child(const_iterator& parent, const tree_type& subtree) {
      node_type node = nullptr;
      if( is_valid(parent) && !subtree.empty() ){
         node = node_allocator.allocate(1, parent.node);
         node_allocator.construct(node,*(subtree.top->next_sibling));
         parent.node->set_last_child(node);
      }
      return node;
   }
   
   iterator append_last_child(const_iterator& parent, tree_type&& subtree) {
      node_type node = nullptr;
      if( is_valid(parent)  && !subtree.empty() ){
         node = subtree.top->next_sibling;
         node->next_sibling = nullptr;
         node->prev_sibling = nullptr;
         parent.node->set_last_child(node);
         subtree.top->next_sibling = subtree.bottom;
         subtree.bottom->prev_sibling = subtree.top;
      }
      return node;
   }
   
   inline iterator append_child(const_iterator& parent, const tree_type& subtree){
      return append_last_child(parent, subtree);
   }
   
   iterator append_next_sibling(const_iterator& sibling, const tree_type& subtree) {
      node_type node = nullptr;
      if( (is_valid(sibling) || empty() && sibling.node==top) && !subtree.empty() ){
         node = node_allocator.allocate(1, sibling.node);
         node_allocator.construct(node,*(subtree.top->next_sibling));
         sibling.node->set_next_sibling(node);
      }
      return node;
   }
   
   iterator append_next_sibling(const_iterator& sibling, tree_type&& subtree) {
      node_type node = nullptr;
      if( (is_valid(sibling) || empty() && sibling.node==top) && !subtree.empty() ){
         node = subtree.top->next_sibling;
         node->next_sibling = nullptr;
         node->prev_sibling = nullptr;
         sibling.node->set_next_sibling(node);
         subtree.top->next_sibling = subtree.bottom;
         subtree.bottom->prev_sibling = subtree.top;
      }
      return node;
   }
   
   iterator append_prev_sibling(const_iterator& sibling, const tree_type& subtree) {
      node_type node = nullptr;
      if( (is_valid(sibling) || empty() && sibling.node==top) && !subtree.empty() ){
         node = node_allocator.allocate(1, sibling.node);
         node_allocator.construct(node,*(subtree.top->next_sibling));
         sibling.node->set_prev_sibling(node);
      }
      return node;
   }
   
   iterator append_prev_sibling(const_iterator& sibling, tree_type&& subtree) {
      node_type node = nullptr;
      if( (is_valid(sibling) || empty() && sibling.node==top) && !subtree.empty() ){
         node = subtree.top->next_sibling;
         node->next_sibling = nullptr;
         node->prev_sibling = nullptr;
         sibling.node->set_prev_sibling(node);
         subtree.top->next_sibling = subtree.bottom;
         subtree.bottom->prev_sibling = subtree.top;
      }
      return node;
   }
   
   inline iterator append_sibling(const_iterator& sibling, const tree_type& subtree){
      return append_next_sibling(sibling, subtree);
   }
   
};

#endif
