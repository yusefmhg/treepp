#include <iostream>
#include "tree.hpp"

int main(int argc, char **argv) {
   tree<int> T(1);
   T.insert_child(T.root(), 2);
   T.insert_child(T.root(), 3);
   tree<int>::depth_iterator it = T.root();
   int um = 1;
   std::cout << *++(it) << std::endl;
   std::cout << *it << std::endl;
   return 0;
}
